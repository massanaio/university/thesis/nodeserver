# nodeserver

NodeServer.js to expose a smart contract through an API

Steps to run:

1) Run Ganache development net

2) Run truffle compile

3) Run truffle migrate

4) Run node server.js
