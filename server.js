const express = require('express');
const app = express();
const port = 3000 || process.env.PORT;
const Web3 = require('web3');
const truffle_connect = require('./connection/app.js');
const bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.post('/addUser', (req, res) => {
  console.log("");
  console.log("**** POST /addUser ****");
  console.log("***********************");
  console.log("");

  let name = req.body.name;
  let documentNumber = req.body.documentNumber;
  let address = req.body.address;
  let sender = req.body.sender;  
  
  console.log("User to be added");
  console.log("----------------");
  console.log("Name: " + name);
  console.log("Document Number: " + documentNumber);
  console.log("Address: " + address);
  console.log("");
  console.log("Sent from account: " + sender);
  console.log("----------------");  
  console.log("");

  truffle_connect.addUser(name, documentNumber, address, sender, (identifier) => {
    res.send(identifier);
  });

});

app.post('/checkUserIsVerified', (req, res) => {
  console.log("");
  console.log("**** POST /checkUserIsVerified ****");  
  console.log("***********************************");
  console.log("");
  
  let userIdentifier = req.body.userIdentifier;
  
  truffle_connect.checkUserIsVerified(userIdentifier, (isVerified) => {
    res.send(isVerified);
  });

});

app.listen(port, () => {

  // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
  truffle_connect.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

  console.log("Express Listening at http://localhost:" + port);

});
