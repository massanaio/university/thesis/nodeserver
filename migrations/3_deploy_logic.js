var IdentityVerifier = artifacts.require("./IdentityVerifier.sol");
var DataStorage = artifacts.require("./DataStorage.sol");

module.exports = function(deployer) {  
  deployer.deploy(IdentityVerifier, DataStorage.address).
  then(() => {
    DataStorage.deployed().then(inst => {
      return inst.allowAccess(IdentityVerifier.address);
    });
  });
};

