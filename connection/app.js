const contract = require('truffle-contract');

const idenityVerifier_artifact = require('../build/contracts/IdentityVerifier.json');
var IdenityVerifier = contract(idenityVerifier_artifact);

const dataStorage_artifact = require('../build/contracts/DataStorage.json');
var DataStorage = contract(dataStorage_artifact);

module.exports = {
    addUser: function (user_name, user_id, user_address, sender, callback) {
    var self = this;

    // Bootstrap the IdenityVerifier abstraction for Use.
    IdenityVerifier.setProvider(self.web3.currentProvider);

    var meta;
    IdenityVerifier.deployed().then(function (instance) {
      meta = instance;
      return meta.addUser(user_name, user_id, user_address, {
        from: sender,
        gas: 1000000
      });
    }).then(function (result) {
      var verificationId = result.logs[0].args.userHash;
      console.log("User stored in the blockchain with Verification ID: " + verificationId);
      callback(verificationId);      
    }).catch(function (e) {
      console.log(e);
      callback("Error");
    });

  },
  checkUserIsVerified: function (userIdentifier, callback) {
    var self = this;

    // Bootstrap the IdenityVerifier abstraction for Use.
    IdenityVerifier.setProvider(self.web3.currentProvider);

    var meta;
    IdenityVerifier.deployed().then(function (instance) {
      meta = instance;
      return meta.isVerified(userIdentifier);
    }).then(function (result) {
      console.log("Is user " + userIdentifier + " verified? " + result);
      callback(result);
    }).catch(function (e) {
      console.log("Error");
      console.log(e);
      callback("ERROR");
    });
  }
}