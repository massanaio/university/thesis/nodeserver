pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

import "./DataStorage.sol";

contract IdentityVerifier {

    DataStorage dataStorage;
   
    event userAddedEvent (
        bytes32 userHash
    );   
       
    constructor(address _dataStorageAddress) public {
        dataStorage = DataStorage(_dataStorageAddress);
    }
       
    function addUser (string memory name, string memory documentNumber, string memory homeAddress) public {        
        DataStorage.User memory _user = DataStorage.User(name, documentNumber, homeAddress, true); 
        bytes32 _hash = hashUser(_user);  

        require(!dataStorage.getUser(_hash).exists, "This user already has en entry in the system."); 
        require(!dataStorage.getDocumentNumber(documentNumber), "This document already exists in the system.");
        dataStorage.setUser(_hash, _user);
        dataStorage.setDocumentNumber(documentNumber);

        emit userAddedEvent(_hash);
    } 
    
    function hashUser(DataStorage.User memory user) private pure returns (bytes32) {
        return sha256(abi.encode(user.name, user.documentNumber, user.homeAddress)); 
    }   

    function isVerified (bytes32 userId) public view returns (bool) {                   
        return dataStorage.getUser(userId).exists;   
    }   
    

}