pragma solidity ^0.5.0;
pragma experimental ABIEncoderV2;

contract DataStorage {
    
    struct User {
        string name;
        string documentNumber;
        string homeAddress;
        bool exists;
    }

    mapping (bytes32 => User) verifiedUsers;   
    mapping (string => bool) uniqueDocumentNumbers;
    mapping(address => bool) accessAllowed;
    
    constructor() public {
        accessAllowed[msg.sender] = true;
    }
    
    modifier platform() {
        require(accessAllowed[msg.sender] == true, "Denied Access");
        _;
    }

    // Access modifiers
    function allowAccess(address _address) platform public {
        accessAllowed[_address] = true;
    }
    
    function denyAccess(address _address) platform public {
        accessAllowed[_address] = false;
    }
    
    //User storage
    function getUser(bytes32 _hash) public view returns(User memory) {
        return verifiedUsers[_hash];
    }

    function setUser(bytes32 _hash, User memory _user) platform public {
        verifiedUsers[_hash] = _user;
    }

    function getDocumentNumber(string memory _number) public view returns(bool) {
        return uniqueDocumentNumbers[_number];
    }
    function setDocumentNumber(string memory _number) platform public {
        uniqueDocumentNumbers[_number] = true;
    }

    
    
}